﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;


namespace Sorting.Test
{
    [TestFixture]
    public class QuicksortTest
    {
        private List<int> _unorderedList;
        private List<int> _expectedList;
        private IEnumerable<int> _result;


        [Test]
        public void SortLinq_SimpleList()
        {
            GivenSimpleList();
            WhenListIsSortedWithLinq();
            Assert.That(_result, Is.EquivalentTo(_expectedList));
        }

        [Test]
        public void SortLinq_ThousandElements()
        {
            GivenList(1000);
            WhenListIsSortedWithLinq();
            Assert.That(_result, Is.EquivalentTo(_expectedList));
        }

        [Test]
        public void Sort_SimpleList()
        {
            GivenSimpleList();
            WhenListIsSorted();
            Assert.That(_result, Is.EquivalentTo(_expectedList));
        }

        [Test]
        public void Sort_HundredThousandElements()
        {
            GivenList(10000);
            WhenListIsSorted();
            Assert.That(_result, Is.EquivalentTo(_expectedList));
        }

        [Test]
        public void SortParallel_HundredThousandElements()
        {
            GivenList(100);
            WhenListIsSortedParallel();
            Assert.That(_result, Is.EquivalentTo(_expectedList));
        }

        private void WhenListIsSorted()
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            Mergesort.SortParallel(_unorderedList);
            _result = _unorderedList;
            sw.Stop();

            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        private void WhenListIsSortedParallel()
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            Quicksort.SortParallelThreshold(_unorderedList, 10);
            _result = _unorderedList;
            sw.Stop();

            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        private void WhenListIsSortedWithLinq()
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            _result = Quicksort.SortLinq(_unorderedList);
            sw.Stop();

            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        private void GivenSimpleList()
        {
            _unorderedList = new List<int>() { 2, 3, 12, 35, 65, 13, 34, 898, 870, 654 };
            _expectedList = new List<int>(_unorderedList);
            _expectedList.Sort();
        }

        private void GivenList(int listSize)
        {
            _unorderedList = new List<int>();

            var randomGenerator = new Random();

            for (var i = 0; i < listSize; i++)
            {
                _unorderedList.Add(randomGenerator.Next());
            }

            _expectedList = new List<int>(_unorderedList);
            _expectedList.Sort();
        }
    }
}
