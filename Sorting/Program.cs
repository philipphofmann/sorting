﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using Sorting.Analyse;
using System.Collections.Generic;

namespace Sorting
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            List<SortAction> sortActions = new List<SortAction>();
            sortActions.Add(new SortAction(Quicksort.Sort, "Not Parallel     "));
            sortActions.Add(new SortAction(Quicksort.SortParallel, "ParallelCPU Depth"));
            sortActions.Add(new SortAction((list) => Quicksort.SortParallelThreshold<double>(list, 0), "Threshold 0      "));
            sortActions.Add(new SortAction((list) => Quicksort.SortParallelThreshold<double>(list, 64), "Threshold 64     "));
            sortActions.Add(new SortAction((list) => Quicksort.SortParallelThreshold<double>(list, 128), "Threshold 128    "));

            //Analyse QuickSort
            var quicksortAnalyser = new SortAnalyser(5, 5, sortActions);

            quicksortAnalyser.sb.Append(string.Format("Quicksort{0}", Environment.NewLine));
            quicksortAnalyser.Analyse(50000);
            quicksortAnalyser.Analyse(100000);
            quicksortAnalyser.Analyse(500000);
            quicksortAnalyser.Analyse(1000000);

            //Save result to csv
            var filepath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fullFileNameQuickSort = string.Format("{0}/{1}", filepath, "testData_quickSort.csv");
            var fileDataQuickSort = quicksortAnalyser.sb.ToString();
            File.WriteAllText(fullFileNameQuickSort, fileDataQuickSort);


            //Analyse MergeSort
            sortActions = new List<SortAction>();
            sortActions.Add(new SortAction(Mergesort.Sort, "Not Parallel     "));
            sortActions.Add(new SortAction(Mergesort.SortParallel, "Parallel CPUDepth"));
            sortActions.Add(new SortAction((list) => Mergesort.SortParallelThreshold<double>(list, 0), "Threshold 0      "));
            sortActions.Add(new SortAction((list) => Mergesort.SortParallelThreshold<double>(list, 64), "Threshold 64     "));
            sortActions.Add(new SortAction((list) => Mergesort.SortParallelThreshold<double>(list, 128), "Threshold 128    "));

            var mergeSortAnalyser = new SortAnalyser(5, 5, sortActions);

            mergeSortAnalyser.sb.Append(string.Format("Mergesort{0}", Environment.NewLine));
            mergeSortAnalyser.Analyse(5000);
            mergeSortAnalyser.Analyse(10000);
            mergeSortAnalyser.Analyse(50000);
            mergeSortAnalyser.Analyse(100000);



            //Save result to csv
            var filepathMergeSort = string.Format("{0}/{1}", filepath, "testData_mergeSort.csv");
            var fileDataMergeSort = mergeSortAnalyser.sb.ToString();
            File.WriteAllText(filepathMergeSort, fileDataMergeSort);

        }
    }
}