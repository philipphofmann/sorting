﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Sorting
{
    public class Quicksort
    {
        /// <summary>
        ///     Sorts the specified list with in-place quicksort.
        /// </summary>
        /// <param name="list">The list.</param>
        public static IList<T> Sort<T>(IList<T> list) where T : IComparable<T>
        {
            return Sort(list, 0, list.Count);
        }

        private static IList<T> Sort<T>(IList<T> list, int left, int right) where T : IComparable<T>
        {
            if (list == null || list.Count <= 1 || left >= right) //Stop recursion
                return list;

            var pivotIdx = Partition(list, left, right);
            Sort(list, left, pivotIdx - 1);
            Sort(list, pivotIdx, right);

            return list;
        }

        /// <summary>
        /// Sorts parallel with quicksort.
        /// </summary>
        /// <typeparam name="T">The type of the list elements</typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static IList<T> SortParallel<T>(IList<T> list) where T : IComparable<T>
        {
            var maxParallelRecursionDepth = ProcessInfo.NumberOfProcessorThreads; //The maximum recursion depth for UserWorkItems.
            return SortParallel(list, 0, list.Count, maxParallelRecursionDepth);
        }

        /// <summary>
        /// Sorts parallel with quicksort.
        /// </summary>
        /// <typeparam name="T">The type of the list elements</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <param name="maxParallelRecursionDepth">The maximum recursion depth for UserWorkItems.</param>
        /// <returns></returns>
        private static IList<T> SortParallel<T>(IList<T> list, int left, int right, uint maxParallelRecursionDepth)
            where T : IComparable<T>
        {
            if (list == null || list.Count <= 1 || left >= right) //Stop recursion
                return list;

            var pivotIdx = Partition(list, left, right);

            if (maxParallelRecursionDepth < 1)
            {
                SortParallel(list, left, pivotIdx - 1, maxParallelRecursionDepth);
                SortParallel(list, pivotIdx, right, maxParallelRecursionDepth);
            }
            else
            {
                maxParallelRecursionDepth--;
                ThreadPool.QueueUserWorkItem(a => SortParallel(list, left, pivotIdx - 1, maxParallelRecursionDepth));
                ThreadPool.QueueUserWorkItem(a => SortParallel(list, pivotIdx, right, maxParallelRecursionDepth));
            }

            return list;
        }

        /// <summary>
        /// Sorts parallel with quicksort.
        /// </summary>
        /// <typeparam name="T">The type of the list elements</typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static IList<T> SortParallelThreshold<T>(IList<T> list, int threshold) where T : IComparable<T>
        {
            return SortParallelThreshold(list, 0, list.Count, threshold);
        }

        /// <summary>
        /// Sorts parallel with quicksort.
        /// </summary>
        /// <typeparam name="T">The type of the list elements</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <param name="maxParallelRecursionDepth">The maximum recursion depth for UserWorkItems.</param>
        /// <returns></returns>
        private static IList<T> SortParallelThreshold<T>(IList<T> list, int left, int right, int threshold)
            where T : IComparable<T>
        {
            if (list == null || list.Count <= 1 || left >= right) //Stop recursion
                return list;

            var pivotIdx = Partition(list, left, right);

            if (Math.Abs(left - right) < threshold)
            {
                SortParallelThreshold(list, left, pivotIdx - 1, threshold);
                SortParallelThreshold(list, pivotIdx, right, threshold);
            }
            else
            {
                ThreadPool.QueueUserWorkItem(a => SortParallelThreshold(list, left, pivotIdx - 1, threshold));
                ThreadPool.QueueUserWorkItem(a => SortParallelThreshold(list, pivotIdx, right, threshold));
            }

            return list;
        }

        /// <summary>
        ///     Partitions the specified list. Moves the pivot (list[left]) to the right position
        ///     and all elements that are smaller than or euqal the pivot to the left
        ///     and all elements that are bigger than the pivot to the right.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="left">The left index</param>
        /// <param name="right">The right index</param>
        /// <returns> The pivot element.</returns>
        private static int Partition<T>(IList<T> list, int left, int right) where T : IComparable<T>
        {
            var start = left;
            var pivot = list[start]; //Select pivot
            left++;
            right--;

            while (true)
            {
                while (left <= right && list[left].CompareTo(pivot) <= 0) //search for left element to swap
                    left++;

                while (left <= right && list[right].CompareTo(pivot) > 0) //search for right element to swap
                    right--;

                if (left > right)
                {
                    list[start] = list[left - 1];
                    list[left - 1] = pivot;

                    return left; //Return pivot
                }

                //Swap
                var temp = list[left];
                list[left] = list[right];
                list[right] = temp;
            }
        }

        /// <summary>
        ///     Sorts the passed enumerable with linq
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static IEnumerable<T> SortLinq<T>(IEnumerable<T> items) where T : IComparable<T>
        {
            if (items.Count() <= 1)
                return items;

            var pivot = items.First();

            var less = from item in items where item.CompareTo(pivot) < 0 select item;
            var same = from item in items where item.CompareTo(pivot) == 0 select item;
            var greater = from item in items where item.CompareTo(pivot) > 0 select item;

            return SortLinq(less).Concat(SortLinq(same)).Concat(SortLinq(greater));
        }
    }

    #region processInfo
    /// Privides a single property which gets the number of processor threads
    /// available to the currently executing process.
    internal static class ProcessInfo
    {
        /// Gets the number of processors.
        /// 
        /// The number of processors.
        internal static uint NumberOfProcessorThreads
        {
            get
            {
                using (var currentProcess = Process.GetCurrentProcess())
                {
                    uint result;

                    {
                        const uint BitsPerByte = 8;
                        var loop = BitsPerByte * sizeof(uint);
                        var processAffinityMask =
                            (uint)currentProcess.ProcessorAffinity;

                        result = 0;
                        while (loop != 0)
                        {
                            loop--;
                            result += processAffinityMask & 1;
                            processAffinityMask >>= 1;
                        }
                    }

                    return (result == 0) ? 1 : result;
                }
            }
        }
    }
    #endregion
}