﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sorting
{
    public class Mergesort
    {
        public static IList<T> Sort<T>(IList<T> input) where T : IComparable<T>
        {
            if (input.Count <= 1)
            {
                return input;
            }

            var midpoint = input.Count / 2;
            IList<T> left = new List<T>(midpoint);
            IList<T> right = new List<T>(midpoint);

            for (var i = 0; i < midpoint; i++)
            {
                left.Add(input[i]);
            }

            for (var i = midpoint; i < input.Count; i++)
            {
                right.Add(input[i]);
            }

            left = Sort(left);
            right = Sort(right);

            return Merge(left, right);
        }

        private static IList<T> Merge<T>(IList<T> left, IList<T> right) where T : IComparable<T>
        {
            var listSize = left.Count + right.Count;
            var result = new List<T>(listSize);

            int leftIndex = 0;
            int rightIndex = 0;

            while (leftIndex < left.Count && rightIndex < right.Count)
            {
                if (left[leftIndex].CompareTo(right[rightIndex]) < 0)
                {
                    result.Add(left[leftIndex]);
                    leftIndex++;
                }
                else
                {
                    result.Add(right[rightIndex]);
                    rightIndex++;
                }
            }

            while (leftIndex < left.Count)
            {
                result.Add(left[leftIndex]);
                leftIndex++;
            }

            while (rightIndex < right.Count)
            {
                result.Add(right[rightIndex]);
                rightIndex++;
            }

            return result;
        }

        public static IList<T> SortParallel<T>(IList<T> input) where T : IComparable<T>
        {
            var maxParallelRecursionDepth = ProcessInfo.NumberOfProcessorThreads; //The maximum recursion depth for UserWorkItems.
            return SortParallel(input, maxParallelRecursionDepth);
        }

        public static IList<T> SortParallel<T>(IList<T> input, uint maxParallelRecursionDepth) where T : IComparable<T>
        {


            if (input.Count <= 1)
            {
                return input;
            }

            var midpoint = input.Count / 2;
            IList<T> left = new List<T>(midpoint);
            IList<T> right = new List<T>(midpoint);

            Parallel.For(0, midpoint - 1, i => left.Add(input[i]));
            Parallel.For(midpoint, input.Count - 1, i => right.Add(input[i]));

            if (maxParallelRecursionDepth < 1)
            {
                left = SortParallel(left); 
                right = SortParallel(right);
            }
            else
            {
                maxParallelRecursionDepth--;
                ThreadPool.QueueUserWorkItem((a) => left = SortParallel(left, maxParallelRecursionDepth));
                ThreadPool.QueueUserWorkItem((a) => right = SortParallel(right, maxParallelRecursionDepth));
            }

            return Merge(left, right);
        }

        public static IList<T> SortParallelThreshold<T>(IList<T> input, int threshold) where T : IComparable<T>
        {
            if (input.Count <= 1)
            {
                return input;
            }

            var midpoint = input.Count / 2;
            IList<T> left = new List<T>(midpoint);
            IList<T> right = new List<T>(midpoint);

            Parallel.For(0, midpoint - 1, i => left.Add(input[i]));
            Parallel.For(midpoint, input.Count - 1, i => right.Add(input[i]));

            if (input.Count < threshold)
            {
                left = SortParallel(left); 
                right = SortParallel(right);
            }
            else
            {
                ThreadPool.QueueUserWorkItem((a) => left = SortParallelThreshold(left, threshold));
                ThreadPool.QueueUserWorkItem((a) => right = SortParallelThreshold(right, threshold));
            }

            return input;
        }


        #region processInfo
        /// Privides a single property which gets the number of processor threads
        /// available to the currently executing process.
        internal static class ProcessInfo
        {
            /// Gets the number of processors.
            /// 
            /// The number of processors.
            internal static uint NumberOfProcessorThreads
            {
                get
                {
                    using (var currentProcess = Process.GetCurrentProcess())
                    {
                        uint result;

                        {
                            const uint BitsPerByte = 8;
                            var loop = BitsPerByte * sizeof(uint);
                            var processAffinityMask =
                                (uint)currentProcess.ProcessorAffinity;

                            result = 0;
                            while (loop != 0)
                            {
                                loop--;
                                result += processAffinityMask & 1;
                                processAffinityMask >>= 1;
                            }
                        }

                        return (result == 0) ? 1 : result;
                    }
                }
            }
        }

        #endregion

    }
}