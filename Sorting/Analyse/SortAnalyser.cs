﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sorting.Analyse
{
    public class SortAnalyser
    {
        private readonly int _testCycles;
        private readonly int _differentLists;
        private List<double> _unorderedList;
        private readonly List<SortAction> _sortAction;
        public StringBuilder sb;


        public SortAnalyser(int testCycles, int differentLists, List<SortAction> sortAction)
        {
            _testCycles = testCycles;
            _differentLists = differentLists;
            _sortAction = sortAction;


            AnalyseResults = new List<AnalyseResult>();
            sb = new StringBuilder();
        }

        public List<AnalyseResult> AnalyseResults { get; private set; }

        public void Analyse(int amount)
        {
            Console.WriteLine("Test with: {0}", amount);
            

            for (var i = 0; i < _differentLists; i++)
            {
                GivenList(amount);

                _sortAction.ForEach(action =>
                {
                    AnalyseSort(action, amount);
                });
                sb.Append(Environment.NewLine);
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        private void AnalyseSort(SortAction action, int amount)
        {
            var analyseAction = new AnalyseAction(_unorderedList, action);
            var tester = new PerformanceAnalyser(analyseAction);
            var timeSpans = tester.MeasureExecTimeWithMetrics(_testCycles);

            //Analyse Performance
            AnalyseResults.Add(new AnalyseResult
            {
                Amount = amount,
                MeasuredTimeSpans = timeSpans,
                SortType = action.StrategyName
            });

            Console.WriteLine("{0} - Average Time in {1} milliseconds", action.StrategyName, tester.AverageTime.TotalMilliseconds);
            var newLine = string.Format("{3};{0};{1}{2}", amount, tester.AverageTime.TotalMilliseconds, Environment.NewLine, action.StrategyName);
            sb.Append(newLine);
        }

      
        private void GivenList(int listSize)
        {
            _unorderedList = new List<double>();
            _unorderedList.Clear();
            _unorderedList = ListFactory.RandomList(listSize);
        }
    }
}