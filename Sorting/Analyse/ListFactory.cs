﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting.Analyse
{
    public static class ListFactory
    {
        public static List<double> SimpleList
        {
            get
            {
                return new List<double>() { 2, 3, 12, 35, 65, 13, 34, 898, 870, 654 };
            }
        }

        public static List<double> RandomList(int listSize)
        {
            var list = new List<double>();

            var randomGenerator = new Random();

            for (var i = 0; i < listSize; i++)
            {
                list.Add(randomGenerator.Next());
            };

            return list;
        }
    }
}
