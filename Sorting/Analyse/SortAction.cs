﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting.Analyse
{
    public class SortAction
    {
        public SortAction(Func<IList<double>, IList<double>> func, string strategyName)
        {
            Function = func;
            StrategyName = strategyName;
        }
        public Func<IList<double>, IList<double>> Function { get; private set; }

        public string StrategyName { get; private set; } 
    }
}
