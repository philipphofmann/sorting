﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting.Analyse
{
    public class AnalyseResult
    {
        public int Amount { get; set; }
        public List<TimeSpan> MeasuredTimeSpans { get; set; }
        public string SortType { get; set; }
    }
}
