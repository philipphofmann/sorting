﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Sorting.Analyse
{

    public class PerformanceAnalyser
    {
        public TimeSpan TotalTime { get; private set; }
        public TimeSpan AverageTime { get; private set; }
        public TimeSpan MinTime { get; private set; }
        public TimeSpan MaxTime { get; private set; }
        public IAnalyseAction AnalyseAction { get; set; }

        public PerformanceAnalyser(IAnalyseAction analyseAction)
        {
            AnalyseAction = analyseAction;
            MaxTime = TimeSpan.MinValue;
            MinTime = TimeSpan.MaxValue;
        }

        /// <summary>
        /// Micro performance testing
        /// </summary>
        public void MeasureExecTime()
        {
            var sw = Stopwatch.StartNew();
            AnalyseAction.DoAction();
            sw.Stop();
            AverageTime = sw.Elapsed;
            TotalTime = sw.Elapsed;
        }


        /// <summary>
        /// Micro performance testing, also measures
        /// max and min execution times
        /// </summary>
        /// <param name="iterations">the number of times to perform action</param>
        public List<TimeSpan> MeasureExecTimeWithMetrics(int iterations)
        {
            var totalTimeSpan = new TimeSpan(0);
            var timeSpans = new List<TimeSpan>(iterations);

            AnalyseAction.Prepare();
            AnalyseAction.DoAction(); //warm up
            for (var i = 0; i < iterations; i++)
            {
                AnalyseAction.Prepare();

                var sw = Stopwatch.StartNew();
                AnalyseAction.DoAction();
                sw.Stop();

                timeSpans.Add(sw.Elapsed);

                var thisIteration = sw.Elapsed;
                totalTimeSpan += thisIteration;

                if (thisIteration > MaxTime) MaxTime = thisIteration;
                if (thisIteration < MinTime) MinTime = thisIteration;
            }

            TotalTime = totalTimeSpan;
            AverageTime = new TimeSpan(totalTimeSpan.Ticks / iterations);

            return timeSpans;
        }

       
    }
}
