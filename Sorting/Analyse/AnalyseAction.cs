﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting.Analyse
{
    public class AnalyseAction : IAnalyseAction
    {
        private readonly List<double> _baseList;
        private List<double> _testList;
        private readonly SortAction _action;

        public AnalyseAction(List<double> list, SortAction action)
        {
            _baseList = list;
            _action = action;
        }
        
        public void Prepare()
        {
            _testList = new List<double>(_baseList);
        }

        public void DoAction()
        {
            _action.Function(_testList);
        }
    }
}
